import parse.pparse

def cli = new CliBuilder(usage: ''' parse [path]

Script which will process files in folder (folder path should be passed to script as a parameter).
 - each file which name started with "a_" should calculate number of "a" character in it and write result as a new line to file
 - each file which starts from "1_" should replace file content with randomly generated matrix 4 x 4 and sum of main diagonal and antidiagonal from new line
 - each file which starts from "d_" should replace file content with date in "YYYY-MM-DD HH-MM-SS" format
 - other file should be ignored
''')
// Create the list of options.
cli.with {
    h longOpt: 'help', 'Show usage information'
}
def options = cli.parse(args)

// Show usage text when -h or --help option is used.
if (!args.size() || options.h) {
    cli.usage()
    return
}


def dir = args[0]
def filesList = new pparse().scan(dir);
println filesList;

//calculate number of "a" character in it and write result as a new line to file
filesList['a'].each{ pparse.processA(dir, it) }

//replace file content with date in "YYYY-MM-DD HH-MM-SS" format
filesList['d'].each{ pparse.processD(dir, it) }

//replace file content with randomly generated matrix 4 x 4 and sum of main diagonal and antidiagonal from new line
filesList['1'].each{ pparse.process1(dir, it) }
