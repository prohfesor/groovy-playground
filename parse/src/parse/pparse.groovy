package parse


class pparse {

    Object scan( basedir ) {
        //array for found files
        def mFiles = [ 'a': [] , 'd': [], '1': []]

        // process all files printing out full name (. and .. auto excluded)
        new File(basedir).eachFile{ f->
            if (f.isFile()){
                switch ( f.name[0,1] ) {
                    case "a_":
                        mFiles['a'].push( f.name );
                        break
                    case "1_":
                        mFiles['1'].push( f.name );
                        break
                    case "d_":
                        mFiles['d'].push( f.name );
                        break
                }
            }
        }

        return mFiles
    }


    static Boolean processA ( directory, filename ) {
        //count occurences
        def count = new File( directory + "//" + filename).getText().count("a");
        //write to file
        def f = new File( directory + "//done_" +filename)
        f.write( count.toString() );
        return true
    }


    static Boolean processD ( directory, filename ) {
        //get timestamp string
        def content = new Date().format("yyyy-MM-dd HH-mm-ss")
        //write to file
        def f = new File( directory + "//done_" +filename)
        f.write( content )
        return true
    }


    static Boolean process1 ( directory, filename ) {
        def matrix = [ ]
        def matrixString = ""
        def diagonal = 0
        def antidiagonal = 0

        //generate matrix
        for(i in 0..3) {
            matrix[i] = []
            for(j in 0..3) {
                matrix[i] << new Random().nextInt(5)
            }
        }
        //count diagonals and convert to string
        for(n in 0..3) {
            diagonal += matrix[n][n]
            antidiagonal += matrix[n][3-n]
            matrixString += matrix[n].join(" ") + "\n"
        }

        //write to file
        def f = new File( directory + "//done_" +filename)
        def content = matrixString + antidiagonal + " + " + diagonal + " = " + (antidiagonal + diagonal)
        f.write( content )
        return true
    }

}