<%@ page import="helloworld.Task" %>



<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'created', 'error')} required">
	<label for="created">
		<g:message code="task.created.label" default="Created" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="created" precision="day"  value="${taskInstance?.created}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="task.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="description" required="" value="${taskInstance?.description}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'done', 'error')} ">
	<label for="done">
		<g:message code="task.done.label" default="Done" />
		
	</label>
	<g:checkBox name="done" value="${taskInstance?.done}" />

</div>

<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'due', 'error')} required">
	<label for="due">
		<g:message code="task.due.label" default="Due" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="due" precision="day"  value="${taskInstance?.due}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="task.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" required="" value="${taskInstance?.title}"/>

</div>

