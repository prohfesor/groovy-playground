package helloworld

class Task {
    static scaffold = true

    String title
    Date due
    Date created
    Boolean done
    String description

    static mapping = {
        description type: "text"
    }

    static constraints = {
    }
}
