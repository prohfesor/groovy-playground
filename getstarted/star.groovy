def a = ['a', 'b', 'c']
def b = a*.toUpperCase()

a_len = a.size()
b_len = b.size()
assert a_len == b_len

el0_a = a.get(0)
el0_b = b[0]
assert el0_a != el0_b

el0_a_u = el0_a.toUpperCase()
assert el0_a_u == el0_b