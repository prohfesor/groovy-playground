r = 1..5

r_l = r.size()
assert r_l == 5

//from doc
assert r instanceof java.util.List
assert r.contains(5)
assert r.from == 1
assert r.to == 5